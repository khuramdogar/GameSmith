/*
 * Availability validation
 */

import { createValidator, required } from 'utils/validation';

const validate = createValidator({
  status: [required],
  availability: [required],
  location: [required],
});

export default validate;
